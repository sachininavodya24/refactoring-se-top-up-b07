import java.awt.*;

import javax.swing.*;

public class PictureFrame {
  public int[] reroll = null;
  Aardvark master = null;
  // constants
  private static final int GRID_SIZE = 20;
  private static final int GRID_OFFSET = 30;
  private static final int GRID_ROWS = 7;
  private static final int GRID_COLS = 8;
  private static final int HEADING_OFFSET = 10;
  private static final int DOMINO_STROKE_WIDTH = 2;
  
  class DominoPanel extends JPanel {
    private static final long serialVersionUID = 4190229282411119364L;

    public void drawGrid(Graphics g) {
        for (int are = 0; are < GRID_ROWS; are++) {
          for (int see = 0; see < GRID_COLS; see++) {
            drawDigitGivenCentre(g, GRID_OFFSET + see * GRID_SIZE, GRID_OFFSET + are * GRID_SIZE, GRID_SIZE,
                master.grid[are][see]);
          }
        }
      }

      public void drawGridLines(Graphics g) {
        g.setColor(Color.LIGHT_GRAY);
        for (int are = 0; are <= GRID_ROWS; are++) {
          g.drawLine(GRID_OFFSET - GRID_SIZE, GRID_OFFSET - GRID_SIZE + are * GRID_SIZE, GRID_OFFSET + GRID_COLS * GRID_SIZE, GRID_OFFSET - GRID_SIZE + are * GRID_SIZE);
        }
        for (int see = 0; see <= GRID_COLS; see++) {
          g.drawLine(GRID_OFFSET - GRID_SIZE + see * GRID_SIZE, GRID_OFFSET - GRID_SIZE, GRID_OFFSET - GRID_SIZE + see * GRID_SIZE, GRID_OFFSET - GRID_SIZE + GRID_ROWS * GRID_SIZE);
        }
      }

      public void drawHeadings(Graphics g) {
        for (int are = 0; are < GRID_ROWS; are++) {
          fillDigitGivenCentre(g, HEADING_OFFSET - GRID_SIZE, GRID_OFFSET + are * GRID_SIZE, GRID_SIZE, are+1);
        }

        for (int see = 0; see < GRID_COLS; see++) {
          fillDigitGivenCentre(g, GRID_OFFSET + see * GRID_SIZE, HEADING_OFFSET - GRID_SIZE, GRID_SIZE, see+1);

    public void drawDomino(Graphics g, Domino d) {
      if (d.placed) {
        int y = Math.min(d.ly, d.hy);
        int x = Math.min(d.lx, d.hx);
        int w = Math.abs(d.lx - d.hx) + 1;
        int h = Math.abs(d.ly - d.hy) + 1;
        g.setColor(Color.WHITE);
        g.fillRect(20 + x * 20, 20 + y * 20, w * 20, h * 20);
        g.setColor(Color.RED);
        g.drawRect(20 + x * 20, 20 + y * 20, w * 20, h * 20);
        drawDigitGivenCentre(g, 30 + d.hx * 20, 30 + d.hy * 20, 20, d.high,
            Color.BLUE);
        drawDigitGivenCentre(g, 30 + d.lx * 20, 30 + d.ly * 20, 20, d.low,
            Color.BLUE);
      }
    }

    void drawDigitGivenCentre(Graphics g, int x, int y, int diameter, int n) {
      int radius = diameter / 2;
      g.setColor(Color.BLACK);
      // g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }

    void drawDigitGivenCentre(Graphics g, int x, int y, int diameter, int n,
        Color c) {
      int radius = diameter / 2;
      g.setColor(c);
      // g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }

    void fillDigitGivenCentre(Graphics g, int x, int y, int diameter, int n) {
      int radius = diameter / 2;
      g.setColor(Color.GREEN);
      g.fillOval(x - radius, y - radius, diameter, diameter);
      g.setColor(Color.BLACK);
      g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }

    protected void paintComponent(Graphics g) {
      g.setColor(Color.YELLOW);
      g.fillRect(0, 0, getWidth(), getHeight());

      // numbaz(g);
      //
      // if (master!=null && master.orig != null) {
      // drawRoll(g, master.orig);
      // }
      // if (reroll != null) {
      // drawReroll(g, reroll);
      // }
      //
      // drawGrid(g);
      if (master.mode == 1) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawGuesses(g);
      }
      if (master.mode == 0) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawDominoes(g);
      }
    }

    public Dimension getPreferredSize() {
      return new Dimension(202, 182);
    }
  }

  public DominoPanel dp;

  public void PictureFrame(Aardvark sf) {
    master = sf;
    if (dp == null) {
      JFrame f = new JFrame("Abominodo");
      dp = new DominoPanel();
      f.setContentPane(dp);
      f.pack();
      f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      f.setVisible(true);
    }
  }

  public void reset() {
    // TODO Auto-generated method stub

  }

}
