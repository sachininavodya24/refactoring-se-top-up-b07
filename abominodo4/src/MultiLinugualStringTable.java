import java.util.HashMap;
import java.util.Map;

public class MultiLinugualStringTable {
  private static Map<String, String[]> messages = new HashMap<>();
  private static String currentLanguage = "English";

  static {
    messages.put("English", new String[] {"Enter your name:", "Welcome", "Have a good time playing Abominodo"});
    messages.put("Klingon", new String[] {"'el lIj pong:", "nuqneH", "QaQ poH Abominodo"});
  }

  public static void setLanguage(String language) {
    currentLanguage = language;
  }

  public static String getMessage(int index) {
    return messages.get(currentLanguage)[index];
  }
}