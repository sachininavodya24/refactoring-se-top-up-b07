import java.net.InetAddress;
import java.io.IOException;
import java.util.ResourceBundle;

public class ConnectionGenius {
  
  private InetAddress ipa;
  private ResourceBundle messages;
  
  public ConnectionGenius(InetAddress ipa) {
    this.ipa = ipa;
    messages = ResourceBundle.getBundle("MessagesBundle");
  }

  public void fireUpGame() {
    try {
      downloadWebVersion();
      connectToWebService();
      awayWeGo();
    } catch (IOException e) {
      System.err.println(messages.getString("error.io") + e.getMessage());
    }
  }
  
  private void downloadWebVersion() throws IOException {
    System.out.println(messages.getString("downloading.webversion"));
    System.out.println(messages.getString("wait"));
    // code to download the web version
  }
  
  private void connectToWebService() throws IOException {
    System.out.println(messages.getString("connecting"));
    // code to connect to the web service
  }
  
  private void awayWeGo(){
    System.out.println(messages.getString("ready.to.play"));
  }

}